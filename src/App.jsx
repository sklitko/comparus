import { useState, useEffect, useRef } from "react";

import "./App.css";
import Board from "./components/Board";
import Modal from "./components/Modal";
import Button from "./components/Button";
import Input from "./components/Input";

const Game = () => {
  const points = 10;

  const [isPlay, setIsPlay] = useState(false);
  const cells = useRef(Array.from({ length: 100 }, (v, i) => i + 1));

  const [activeCell, setActiveCell] = useState(0);
  const [userCell, setUserCell] = useState([]);
  const [compCell, setCompCell] = useState([]);

  const [time, setTime] = useState(1000);
  const [showModal, setShowModal] = useState(false);

  const timerIdRef = useRef();

  const generateUniqueRandomNumber = () => {
    if (userCell.length + compCell.length >= cells.current.length) return;
    let randomNumber;
    do {
      randomNumber = Math.floor(Math.random() * cells.current.length) + 1;
    } while (userCell.includes(randomNumber) || compCell.includes(randomNumber));
    return randomNumber;
  };

  const resetRound = () => {
    clearTimeout(timerIdRef.current);
  };

  const checkGameOver = () => {
    const isFinish = userCell.length === points - 1 || compCell.length === points - 1;
    if (isFinish) {
      setIsPlay(false);
      clearTimeout(timerIdRef.current);
      setShowModal(true);
    }
    resetRound();
  };

  const handleTimeout = (cell) => {
    setCompCell((prev) => [...prev, cell]);
    checkGameOver();
  };

  const handleCellClick = (cell) => {
    if (cell === activeCell) {
      setUserCell((prev) => [...prev, cell]);
      checkGameOver();
    }
  };

  const handleStart = () => {
    setActiveCell(0);
    setUserCell([]);
    setCompCell([]);
    setIsPlay(true);
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  useEffect(() => {
    if (!isPlay) return;

    const randomCellIndex = generateUniqueRandomNumber();
    setActiveCell(randomCellIndex);
    timerIdRef.current = setTimeout(() => handleTimeout(randomCellIndex), time);

    return () => clearTimeout(timerIdRef.current);
  }, [isPlay, userCell, compCell]);

  return (
    <div className="container">
      <img src="/comparus.svg" alt="comparus" className="logo" />
      <Board
        cells={cells.current}
        userCell={userCell}
        compCell={compCell}
        activeCell={activeCell}
        onClick={handleCellClick}
      />
      <div className="wrapper">
        <Input
          placeholder={"Час у мілісекундах"}
          value={time}
          onChange={(e) => setTime(e.target.value)}
          disabled={isPlay}
        />
        <Button title={"Почати"} onClick={handleStart} disabled={!time} />
      </div>
      <p>
        Рахунок: Гравець {userCell.length} - {compCell.length} Комп&apos;ютер
      </p>
      <Modal isOpen={showModal} onClose={handleCloseModal}>
        <div>Гра завершена! Результат: {userCell.length > compCell.length ? "Ви виграли!" : "Комп'ютер виграв!"}</div>
      </Modal>
    </div>
  );
};

export default Game;
