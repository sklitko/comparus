import Button from "../Button";
import clsx from "clsx";
import "./modal.css";
const Modal = ({ isOpen, onClose, children }) => {
  return (
    <div className={clsx("modal-overlay", isOpen && "open")}>
      <div className={clsx("modal", isOpen && "open")}>
        <div className="modal-content">{children}</div>
        <Button onClick={onClose} title={"Закрити"} />
      </div>
    </div>
  );
};

export default Modal;
