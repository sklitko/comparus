import clsx from "clsx";
import "./board.css";
const Board = ({ cells, activeCell, userCell, compCell, onClick }) => {
  return (
    <div className="board">
      {cells.map((cell) => (
        <div
          key={cell}
          className={clsx(
            "cell",
            activeCell === cell && "yellow",
            userCell.includes(cell) && "green",
            compCell.includes(cell) && "red"
          )}
          onClick={() => onClick(cell)}
        />
      ))}
    </div>
  );
};

export default Board;
