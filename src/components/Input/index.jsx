import "./input.css";
const Input = ({ type = "text", placeholder, value, onChange, disabled }) => {
  return <input type={type} placeholder={placeholder} value={value} onChange={onChange} disabled={disabled} />;
};

export default Input;
