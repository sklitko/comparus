import "./button.css";

const Button = ({ onClick, title, disabled }) => {
  return (
    <button onClick={onClick} disabled={disabled}>
      {title}
    </button>
  );
};

export default Button;
